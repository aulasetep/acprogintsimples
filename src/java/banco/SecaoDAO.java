/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kelvin.souza
 */
public class SecaoDAO extends Banco{
    
    public List<SecaoDTO> listar() {
        ArrayList<SecaoDTO> secoes = new ArrayList<SecaoDTO>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM secao");

            while (resultSet.next()) {
                SecaoDTO s = new SecaoDTO();
                s.setNomeSecao(resultSet.getString("nomeSecao"));
                s.setIdSecao(resultSet.getInt("idSecao"));
    
                secoes.add(s);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
        return secoes;
    }
    
    
    
}
