/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kelvin
 */
public class UsuarioDAO extends Banco {

    public List<UsuarioDTO> listar() {
        ArrayList<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM USUARIO");

            while (resultSet.next()) {
                UsuarioDTO usr = new UsuarioDTO();
                usr.setCpf(resultSet.getString("CPF"));
                usr.setNome(resultSet.getString("NOME"));
                usr.setEmail(resultSet.getString("EMAIL"));
                usr.setSenha(resultSet.getString("SENHA"));
                usuarios.add(usr);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
        return usuarios;
    }

}
