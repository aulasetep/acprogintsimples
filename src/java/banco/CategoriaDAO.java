/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kelvin
 */
public class CategoriaDAO extends Banco{
        
    
    public List<CategoriaDTO> listar() {
        ArrayList<CategoriaDTO> categorias = new ArrayList<CategoriaDTO>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM CATEGORIA");

            while (resultSet.next()) {
                CategoriaDTO cat = new CategoriaDTO();
                cat.setId(resultSet.getInt("ID"));
                cat.setNome(resultSet.getString("NOME"));               
                categorias.add(cat);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
        return categorias;
    }
    
}
