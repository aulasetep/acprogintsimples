/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kelvin
 */
public class ProdutoDAO extends Banco{
    
    
    
    public void incluir(ProdutoDTO e) {
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            connection = getConnection();
            statement = connection.prepareStatement(
                    "INSERT INTO PRODUTO (idSecao, nomeProduto, valor, descricao, imagem, Oferta) VALUES ( ?, ?, ?, ?, ?, ?)   ");
            statement.setInt(1, e.getIdSecao());
            statement.setString(2, e.getNomeProduto());
            statement.setInt(3, e.getValor());
            statement.setString(4, e.getDescricao());
            statement.setString(5, ((e.getImgPath()==null)?"":e.getImgPath()));
            statement.setInt(6, e.getOferta());
            statement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
    }
    
    
        public void excluir(ProdutoDTO p) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.prepareStatement(
                    "DELETE FROM Produto WHERE idProduto = ?");
            statement.setInt(1, p.getIdProduto());
            statement.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
    }
    
    
        public void alterar(ProdutoDTO prod) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = getConnection();
            statement = connection.prepareStatement(
                    "UPDATE PRODUTO SET   idSecao = ?,"
                                        + " nomeProduto = ?,"
                                        + " valor = ?,"
                                        + " descricao = ?,"
                                        + " imagem = ?,"
                                        + " Oferta = ?  WHERE idProduto =  ? ");
            statement.setInt(1, prod.getIdSecao());
            statement.setString(2, prod.getNomeProduto());
            statement.setInt(3, prod.getValor());
            statement.setString(4, prod.getDescricao());
            statement.setString(5, ((prod.getImgPath()==null)?"":prod.getImgPath()));
            statement.setInt(6, prod.getOferta());
            statement.setInt(7, prod.getIdProduto());
            
            statement.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
    }
        
        
    
    public ProdutoDTO getProduto(int idProduto){
        ProdutoDTO prod = new ProdutoDTO();
        
        Connection connection = null;
        Statement statement = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM produto where idProduto = " + idProduto);

            while (resultSet.next()) {
                prod.setIdProduto(resultSet.getInt("idProduto"));
                prod.setIdSecao(resultSet.getInt("idSecao"));
                prod.setNomeProduto(resultSet.getString("nomeProduto"));
                prod.setValor(resultSet.getInt("valor"));
                prod.setDescricao(resultSet.getString("descricao"));
                prod.setImgPath(resultSet.getString("imagem"));
                prod.setOferta(resultSet.getInt("Oferta"));
//                prod.setId(resultSet.getInt("id"));
//                prod.setDestaque(resultSet.getBoolean("destaque"));
//                prod.setNome(resultSet.getString("nome"));
//                prod.setPreco(resultSet.getDouble("preco"));
//                prod.setIdcategoria(resultSet.getInt("id_categoria"));
                
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
        
        return prod;
        
    }    
        
        
    public List<ProdutoDTO> listar() {
        ArrayList<ProdutoDTO> produtos = new ArrayList<ProdutoDTO>();
        Connection connection = null;
        Statement statement = null;
        try {
            connection = getConnection();
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM produto");

            while (resultSet.next()) {
                ProdutoDTO prod = new ProdutoDTO();
                prod.setIdProduto(resultSet.getInt("idProduto"));
                prod.setIdSecao(resultSet.getInt("idSecao"));
                prod.setNomeProduto(resultSet.getString("nomeProduto"));
                prod.setValor(resultSet.getInt("valor"));
                prod.setDescricao(resultSet.getString("descricao"));
                prod.setImgPath(resultSet.getString("imagem"));
//                prod.setOferta(resultSet.getInt("Oferta"));
//                prod.setId(resultSet.getInt("id"));
//                prod.setDestaque(resultSet.getBoolean("destaque"));
//                prod.setNome(resultSet.getString("nome"));
//                prod.setPreco(resultSet.getDouble("preco"));
//                prod.setIdcategoria(resultSet.getInt("id_categoria"));
                produtos.add(prod);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.toString());
            }
        }
        return produtos;
    }
    
}
