/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kelvin
 */
public class Banco {
    
    
    private static final String DRIVER
            = "com.mysql.jdbc.Driver";
    private static final String DATABASE_URL
            = "jdbc:mysql://localhost/ed2web";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(DRIVER);
            connection
                    = DriverManager.getConnection(DATABASE_URL,
                            "root", "ALUNOS");
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
        return connection;
    }   
    
}
    
    
    

