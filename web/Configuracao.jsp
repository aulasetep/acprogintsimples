<%-- 
    Document   : Configuracao
    Created on : Nov 25, 2018, 7:50:40 PM
    Author     : Kelvin
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="banco.SecaoDTO"%>
<%@page import="banco.ProdutoDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="js/validator.min.js"></script>



        <script type="text/javascript">
            function excluirProduto(id) {
                window.location.href = "DeletarProduto?idProduto=" + id;
            }

            function alterarProduto(id) {
                window.location.href = "AlterarProduto?flag=1&idProduto=" + id;
            }


        </script>


        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>JSP Page</title>
    </head>
    <body>
        <%
            ProdutoDTO p = (ProdutoDTO) session.getAttribute("produto");
            String cmd = (String) session.getAttribute("comando");
            String idProd = (String) session.getAttribute("idProdutoAlt");

            List<ProdutoDTO> produtos = (List<ProdutoDTO>) session.getAttribute("produtos");
            Map s = (Map) session.getAttribute("secoes");

            session.setAttribute("idProduto", session.getAttribute("idProduto"));


        %>

        <div class="container bg-light">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Oliveira Games</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent1">
                    <ul class="navbar-nav ml-md-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="Inicio">Início</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Configuração<span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Carrinho</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Minha Conta</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Contato</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Login</a>
                        </li>


                    </ul>

                </div>
            </nav>

        </div>   

        <div class="container">

            <h3 class="h-100" style="margin-top: 30px; margin-bottom: 40px; ">
                Configuração dos Produtos<br/>
                <small class="text-muted">Adicione, remova e altere</small>
            </h3>

        </div>

        <!-- Controle de produtos -->
        <div class = "container">
            <form method='POST' action='AlterarIncluir' data-toggle="validator" >
                <div class='row'>
                    <div class='col '>
                        <div class="form-row align-items-center">
                            <div class="form-group col-md-2">
                                <label for="idProd">Id:</label>
                                <input class="form-control" type="text" name = "idProd" value = "<% out.print(idProd);%>" placeholder="<% out.print(idProd);%>" readonly>
                            </div>
                            <div class="form-group col-md-10">
                                <label for="nomeProduto">Nome:</label>
                                <input type="text" class="form-control" id="nomeProduto" name="nomeProduto" <%out.print((p.getNomeProduto() == null) ? "" : "value = '" + p.getNomeProduto() + "'"); %> placeholder="Nome do produto" required>                
                            </div>

                            <div class="form-group col-md-3">
                                <label for="valorProduto">Valor:</label>
                                <input type="number" min="0" class="form-control" id="valorProduto" name="valorProduto" <%out.print((p.getValor() == 0) ? "" : " value='" + p.getValor() + "'"); %> placeholder="0,00" required>                
                            </div>                    
                            <div class="form-group col-md-4">
                                <label for="idSecao">Seção:</label>
                                <select class="form-control" id="idSecao" name="idSecao" <%out.print((p.getIdSecao() == 0) ? "" : " value='" + p.getIdSecao() + "'"); %> required>
                                    <option <%out.print((p.getIdSecao() == 0) ? "selected" : "");%> selected>Escolha</option> 

                                    <%for (Object sec : s.keySet()) {%>

                                    <option value = '<%out.print(sec);%>' <%out.print((p.getIdSecao() == Integer.parseInt(sec.toString())) ? " selected " : "");%>> <%out.print(s.get(sec).toString());%> </option>                            


                                    <% }%>


                                </select>
                            </div>
                            <div class="col-md-2">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="oferta" value="1" name="oferta" <%out.print((p.getOferta() == 0) ? " " : " checked"); %>/>
                                    <label class="form-check-label" for="oferta">
                                        Oferta
                                    </label>
                                </div>
                            </div>   

                            <div class="form-group col-md-12">

                                <label for="descProduto">Descrição:</label>
                                <textarea class="form-control" id="descProduto" name="descProduto" rows="3" required><% out.print((p.getDescricao() == null) ? "" : p.getDescricao());  %></textarea>
                            </div>

                        </div>
                    </div>


                    <div class='col'>
                        <div class="form-row align-items-center">
                            
                            <div class="form-group col-md-12">
                                <div class='container' style="height: 180px; width: 180px;margin-top: 10px;"><img src='<%out.print((p.getImgPath()== null) ? "" : p.getImgPath());%>' class="img-fluid" ></div>
                            </div>
                                                                 
                            
                            <div class="form-group">
                                <div class="container">
                                    <label for="imgPath"><%out.print((p.getImgPath()== null) ? "Caminho da imagem" : p.getImgPath());%></label>
                                    <input type="file" class="form-control-file" name="imgPath" id="imgPath" value = '<% out.print((p.getImgPath() == null) ? "" : p.getImgPath());%>'>
                                </div>
                            </div>
                        </div>

                    </div>      

                </div>            
                                

                <div class="form-row align-items-center">



                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-primary" name="button" value='<% out.print((cmd.equals("incluir")) ? "incluir" : (cmd.equals("alterar")) ? "alterar" : "");  %>'><% out.print((cmd.equals("incluir")) ? "Incluir" : (cmd.equals("alterar")) ? "Alterar" : "");%></button>
                    </div>
                </div>




            </form>
        </div>


        <div class='container'>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Imagem</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Valor</th>
                        <th scope="col">Secao</th>
                        <th scope="col">Descrição</th>
                        <th scope="col" colspan = "2" style="text-align: center;">Ação</th>


                    </tr>
                </thead>
                <tbody>
                    <% for (ProdutoDTO prod : produtos) { %>

                    <tr>
                        <td><div class='container' style="height: 100px; width: 100px;"><img src='<%out.print(prod.getImgPath());%>' class="img-fluid" ></div></td>
                        <td><%out.print(prod.getNomeProduto());%></td>
                        <td><%out.print(prod.getValor());%></td>
                        <td><%out.print(s.get(prod.getIdSecao()));%></td>
                        <td><%out.print(prod.getDescricao());%></td>
                        <td>
                            <div class="form-group col-md-2">
                                <button type="button" class="btn btn-primary btn-outline-danger btn-sm" onclick="alterarProduto('<%out.print(prod.getIdProduto());%>')">Alterar</button>
                            </div>
                        </td>

                        <td>
                            <div class="form-group col-md-2">
                                <button type="button" class="btn btn-primary btn-outline-danger btn-sm" onclick="excluirProduto('<%out.print(prod.getIdProduto());%>')">Remover</button>
                            </div>
                        </td>
                    </tr>

                    <%}%>
                </tbody>
            </table>




        </div>





        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



    </body>
</html>
